# Tyran Name Translator

_Tyran Name Translator_ est un script pour greasemonkey/tampermonkey pour traduire les nom des joueurs IG par leurs pseudo.

## Comment installer

1. Avoir [tampermonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo) (chrome) ou [greasemonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) (firefox) installer sur votre naviguateur.
2. [Installer le script](https://gitlab.com/foooo/tnr/raw/master/tyran_name_translator.user.js)

## Comment mettre à jour

Normalement les scripts font leurs mise à jour automatiquement d'ici maximum 7j.

### Greasemonkey

Cliquer sur greasemonkey > Tyran Name Translator > Option du script utilisateur > Mettre à jour maintenant.

![greasemonkey update script](https://i.mlgimg.xyz/df11b3dc.png)

### Tampermonkey

Cliquer sur tampermonkey > Vérifier les mises à jour d'userscript.

![tampermonkey update script](https://i.mlgimg.xyz/a5ad416f.png)
