// ==UserScript==
// @name     Tyran Name Translator
// @version  0.2.5
// @grant    GM.setValue
// @grant    GM.getValue
// @include  https://*.travian.*/*
// @require  https://gitlab.com/foooo/tnr/raw/master/tippy.min.js
// @run-at   document-idle
// ==/UserScript==

let values = {
  Illlllllll: "Cro/Drazh/Max",
  lllIIIIIII: "Sheepman/Adrix",
  llllllllll: "Antho/Cliff/Gwen/Mimille",
  lIlIlIlIlI: "Pat/Tinno/Kav",
  IIllllllll: "Choll",
  IIIlllllll: "Death/Dub/Pabs",
  IIIIllllll: "Sad/Crassus",
  IIIIIlllll: "Geo/Maz",
  llIIllIIll: "Tatoo/Servane",
  IIIIIIIlll: "Alkyna",
  IIIIIIIIll: "Sweet",
  IIIIIIIIIl: "Martin",
  IIIIIIIIII: "Bidou",
  lllIIIIIll: "Sidi",
  IIllllIIII: "Amposta",
  llllIIIIII: "Maleur",
  lllllIIIII: "Fremen",
  llllIIIIll: "Doc",
  lllllllIII: "Julian",
  lllllllllI: "Glover",
  IllllllllI: "Wolf",
  IIlllllllI: "Cédric/Fitzou/Dédé",
  IIIllllllI: "Martine/Nicolas/VoY",
  IIIIlllllI: "Sludge",
  IIIIIllllI: "FreeJustice",
  IIIIIIlllI: "Nonos/PsY4",
  IIIIIIIIlI: "Sphynx/Tarzan",
  lIIIIIIIIl: "Iroih",
  llIIIIIIIl: "Dall'Agnolo",
  lllIIIIIIl: "Spartan",
  llllIIIIIl: "Sinistre",
  lllllIIIIl: "Docteur Who/evilbled/Emmanuel",
  llllllIIIl: "Le Bide",
  lllllllIIl: "Shiho",
  llllllllIl: "Alleria/Fitz",
  llIIIllIlI: "Zeiko",
  IIllllllII: "CoCoRiCo",
  IIIlllllII: "Ness",
  IIIIllllII: "Kat/Lomeso",
  IIIIIlllII: "Lordstark/pigeon",
  IIIIIIllII: "lolo",
  IIIIIIIlII: "Ahz/Tommy",
  IIllIIllII: "Gazolyn",
  lIlIllllll: "Antoine",
  lIlIIlllll: "Hans"
};

TravianShowName(values);

function TravianShowName(names){
  Object.entries(names).forEach(([igName, persons]) => {
    contains("h1,p,span,a", igName).forEach(el => {
      tippy(el, {content: persons, touchHold: true});
    });
  });

  function contains(selector, text) {
    var elements = document.querySelectorAll(selector);
    return Array.prototype.filter.call(elements, function(element){
      return RegExp(text).test(element.textContent);
    });
  }
}
